#!/usr/bin/env python
""" 
 classify a scaffold 
 uses a series of pre trained models (kfolds), 
 run prediction with each model, then average the score
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Oracle_Mito import Oracle_Mito
import yaml
from pprint import pprint

from Util_Mito import load_multi_scaffold_fasta

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='recommend best HK-RR pairs for given species',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--project",
                        default='mito3',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("--fastaPath",
                        default='fixMe3',help="path to input fasta files")

    parser.add_argument("--scaffName",
                        default='fixMe4',help="full scaffold name +.standard.")

    parser.add_argument("--outPath",
                        default='out',help="output path for plots/output")
 
    parser.add_argument("-n", "--events", type=int, default=200,
                        help="num seqences for classification")
 
    parser.add_argument("--seedModel",
                        default='/global/cscratch1/sd/balewski/mito3_sum/3a/mito3a-',
                        help="trained model and weights")

    parser.add_argument("-k","--kfoldList",nargs="+",
                        default=['20'],
                        help=" blank separated list of kfold IDs, takes n1-n2")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
     
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

scaffN=args.scaffName

ora=Oracle_Mito(args)


fullPath=args.fastaPath+'/'+scaffN+'.scaffolds.fasta'

scaffD=load_multi_scaffold_fasta(fullPath,ora.basesSet,  maxRnd=3000)
print('M: num scaffolds:', len(scaffD))
#print([x[0] for x in scaffD])
#exit(3)
ora.load_models(path=args.seedModel,kL=args.kfoldList)

#print(list(scaffD))
for scaffN in scaffD:
    role,seqStr=scaffD[scaffN]
    print(role,scaffN,len(seqStr))
    seqL=ora.sample_scaffold(seqStr,args.events)

    #seqStr=ora.load_one_scaffold_fasta(fullPath)

    rec={'data_info':{'path':args.fastaPath,'size':len(seqStr),'scaffName':scaffN,'prior_role':role}}
    Xhot=ora.build_data_one(seqL)
    print('got', len(seqL),Xhot.shape)
    rec['ora_inp']={'nSample':Xhot.shape[0],'seqLen':Xhot.shape[1]}
    Yscore,avr_str,rec1=ora.classify_one_scaffold(Xhot)
    decision='MITO' if rec1['avr']>0.5 else 'MAIN'
    print(scaffN,decision,'=decision, avr score:', avr_str)
    rec['score']=rec1
    rec['model_info']=ora.info
    #print('out rec='); pprint(rec)

    outF=args.outPath+'/%s.oracle.yml'%scaffN
    print('save yaml:',outF)
    ymlf = open(outF, 'w')
    yaml.dump(rec, ymlf)
    ymlf.close()
    #exit(4)
