#!/usr/bin/env python
""" 
 evaluates one kfold for  mito-main classifier
 output: yaml
 uses pre trained net
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Deep_Mito import Deep_Mito

import time
import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='recommend best HK-RR pairs for given species',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--project",
                        default='mito3',dest='prjName',
                        help="core name used to store outputs")
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")

    parser.add_argument("--fastaPath",
                        default='/global/projectb/scratch/andreopo/NN/read-autoencoder/NN_sequence_classify/MITO/CROSS-VALIDATION_allfungalstd/',help="path to input fasta files")
    parser.add_argument("--dataPath",
                        default='data',help="path to input")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots/output")

    parser.add_argument("-k", "--kfoldOffset", type=int, default=-1,
                        help="decides which segments merge for training")

    parser.add_argument("-n", "--events", type=int, default=10000*8,
                        help="num HK-RR pairs for training, use 0 for all")    

    parser.add_argument("--seedModel",
                        default='out/',
                        help="seed model and weights")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep=Deep_Mito(args)
for role in deep.mr12:
    deep.prep_labeled_input(role,'val')
    deep.prep_labeled_input(role,'test')
deep.build_training_data()

deep.load_models(path=args.seedModel)

domL=['test','val']
out={}
for dom in domL:
    start = time.time()
    print('\n------ process dom:',dom)
    y_true,y_score,loss, acc,roc_auc=deep.model_predict_domain(dom)
 
    nAll=y_true.shape[0]
    L1=int(y_true.sum())
    L0=nAll - L1
   
    print('  domain done, kfold=%d loss=%.3f, acc=%.3f, AUC=%.3f   elaT=%.1f sec'%(args.kfoldOffset,loss,acc,roc_auc,time.time() - start))

    out[dom]={'accuracy':acc,'AUC':roc_auc,'loss':loss,'kfold_offset':args.kfoldOffset,'nNeg':L0,'nPos':L1,'idx':args.arrIdx}


import yaml
outF=deep.outPath+'/kfold_idx%d.yml'%args.arrIdx
print('save yaml:',outF)
ymlf = open(outF, 'w')
yaml.dump(out, ymlf)
ymlf.close()


