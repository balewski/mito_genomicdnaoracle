#!/usr/bin/env python
"""  read raw input data
sanitize, split, ballance correction
write data as tensors as 10 segments
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

exit(1)  # block 3a
from Plotter_Mito import Plotter_Mito
from Deep_Mito import Deep_Mito

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Format raw scafolding  data for Mito Oracle training',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--project",
                        default='mito3',dest='prjName',
                        help="core name used everywhere")
    parser.add_argument("--fastaPath",
                        default='/global/projectb/scratch/andreopo/NN/read-autoencoder/NN_sequence_classify/MITO/CROSS-VALIDATION_allfungalstd/',help="path to input fasta files")
    parser.add_argument("--dataPath",
                        default='data',help="path to ML input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots")
    parser.add_argument( "-X","--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    args.arrIdx=0 # for plotter, not needed here
    args.kfoldOffset=0 # for training, not needed here
    args.events=0 # for training, not needed here
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

#ppp=Plotter_Mito(args )
deep=Deep_Mito(args)
deep.numSegm=30 # we create more segments because there is too much data
rec=deep.scan_fasta_dir()
deep.split_species(rec,'mito')
deep.split_species(rec,'main')

#  OLD 
exit(1)

ppp.pause(args,'format')

