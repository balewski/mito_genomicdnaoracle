#!/usr/bin/env python
""" 
read yaml predictions for a species and publish it on the web
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


from PubAll_Mito import PubAll_Mito

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='publish mito/main classification on the web',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--project",
                        default='mito3',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("--dataPath",
                        default='out',help="path to input")
    parser.add_argument("--webPath",
                        default='/project/projectdirs/mpccc/www/balewski/tryAny/mito3/',help="output path for plots")
    

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.deepName='Mito Oracle v3.1'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
#args.webPath='web/'

specNameL=['1127646.Earliella_scabrosa_CIRM-BRFM_1817.standard','1021558.Lecythophora_sp._PMI_546.standard','1029422.Paraconiothyrium_sporulosum_AP3s5-JAC2a.standard']


#pub=PubAll_Mito(args ,specNameL=specNameL[:])
pub=PubAll_Mito(args , maxSpec=10)
#pub=PubAll_Mito(args )

pub.species_loop()
pub.summary()

pub.coverHTML()

if 0: # read species from data-split segments
    import yaml
    inpF='data/mito3.spec-split-main.yml'
    ymlf = open(inpF, 'r')
    inpD=yaml.load( ymlf, Loader=yaml.CLoader)
    ymlf.close()
    nSeg=len(inpD)
    print(' found %d split-segments:'%nSeg,list(inpD.keys()))
    specNameL=[]
    for seg in range(10,30):
        specNameL+=inpD[seg].keys()

    print('gregated specN list', specNameL,len( specNameL))

#exit(1)

