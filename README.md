## README 

Separating mitochondrial vs. genomic DNA using ML


### Quick summary

The model sonsisting of 2 layer LSTMT followed by 2 Dense layers is encoded in TensforFlow & Keras 
Training is based on labeled 150 base-pairs obtained from shreaded skaffold fasta data. Full training takes few hours on 10+ CPU machine.


There are 3 'main' programs:

**format_Mito.py** - prepares input data

**train_Mito.py**  - traines the model

**predict_Mito.py** - predicts, bases on training
**classify_Mito.py** - make binary decision for single scaffold

 ./classify_Mito.py --fastaPath $xpath  --scaffName $fname --kfoldList $kfold


Bulk evaluation for series of scaffold files:
  challange1.sh*  challange2.sh*


Evaluate kfold training
bigLoop_kfoldOne.sh*  + kfold_one_eval.py + plot_kfold_avr.py

All programs support run time arguments , -h lists all optinos.


* Version: 3


### How do I get set up? ###

* Summary of set up
On Cori load Keras & Tensorflow with:
```bash
cori01:$  module load python/3.6-anaconda-4.4 
```

* Input fasta files are NOT included in this repo, they are owned by Bill:
``` bash
/global/projectb/scratch/andreopo/NN/read-autoencoder/NN_sequence_classify/MITO/CROSS-VALIDATION_allfungalstd/  101GB
- 399 main  (exclude 'ALL')
- 385 mito
```

* Input data segments. It is too much for the training so the decision was made to split data randomly on to 20 segments and use only segments 0-9 for the training. The other segments 10-19 can be used for testing of the final model.

Each segment contains approximately equall length of mito & main aminoaccids, butonly within each categgory. It may be we have much more main data.

```bash
data/

```
Note1, the imput training data are balanced by definition, because only samples of scaffolds are used.

Note2, scaffold samplig is done in fly, duting training initialization, is not reproducible.


### Program: format_Mito.py

It checks sanity of input data and split species into 10 segments with similar number of mito data.

* Execution takes ~10 seconds.
The output file is saved in data/ as yaml:
```bash
$ head  data/mito2.spec-split.yml
0: [fungus4, fungus54, fungus78, fungus80, fungus99, fungus102, fungus113, fungus123,
  fungus131, fungus19]
1: [fungus2, fungus8, fungus32, fungus44, fungus64, fungus86, fungus90, fungus107,
<snip>
```

### Program: train_Mito.py

Implements the model sonsisting of 2 layer LSTMT followed by 2 Dense layers, total 7.3k parameters.
```bash
build_model inpA: (?, 150, 4)
Dens act= relu  recurDropFrac= 0.1  layerDropFrac= 0.2 
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
DNA_seq (InputLayer)         (None, 150, 4)            0         
_________________________________________________________________
A1_30 (LSTM)                 (None, 150, 30)           4200      
_________________________________________________________________
A2_15 (LSTM)                 (None, 15)                2760      
_________________________________________________________________
D_10 (Dense)                 (None, 10)                160       
_________________________________________________________________
dropout_1 (Dropout)          (None, 10)                0         
_________________________________________________________________
D_5 (Dense)                  (None, 5)                 55        
_________________________________________________________________
dropout_2 (Dropout)          (None, 5)                 0         
_________________________________________________________________
decision_1 (Dense)           (None, 1)                 6         
=================================================================
Total params: 7,181.0
```

* run training
$ ./train_Mito.py

This program has many options, by default it will skip most of data (--events 5000) , create under-sampled data, run nly 2 epochs with large batch size of 1000.
It will use first 8 kfolds for training, 8-th kfold for eval and will ignore 9-th kflold.

It produces plost with loss, accuracy, AUC at the end of the job.
It saves final (trained) model as well as best weights.
It always uses balanced data for training and evaluation.

All output files are saved at out/ unless --outPath  is used.
```bash
ls out/
  idx1_mito2_train_f10.pdf  mito2.model.h5 idx1_mito2_train_f11.pdf  val_natur.AUC.csv	val_under.AUC.csv
```

1) to run none trivial training on all data , undersampled, 10 epochs do:
```bash
$  ./train_Mito.py --events 0 --batch_size 100  --epochs 10
```

This mode needs 3GB of RAM, can use up to 6-7 CPUs on Cori login node.
One undersampled epoch takes 130 sec, 
total training takes ~25 minutes.
```bash
Epoch 2/10
133s - loss: 0.4280 - acc: 0.8239 - val_loss: 0.3472 - val_acc: 0.8581
<snip>
Epoch 10/10
130s - loss: 0.3228 - acc: 0.8844 - val_loss: 0.2873 - val_acc: 0.8869
AUC: 0.948139 , save: out/val_under.AUC.csv
```

2) to run on the hard training on oversampled data takes 20x longer per epoch. You need 5 GB RAM.

The AUC curve shows that model will ratian 96% of mito-DNA and will reject 95% of genomic DNA  leading to 1:19 puriffication (the top right plot)

Figure below show example of 8 hours long training
![GitHub Logo](doc/idx12_mito2_train_f10.pdf)

This is how you can do it interactively
```bash
$  ./train_Mito.py --events 0 --batch_size 100  --epochs 10 --arrIdx 10 
```

This is designed to work with slurm job arrays, if array index>=10 then this gets changed in ./train_Mito.py:
```bash 
bala='under' 
if args.arrIdx >10: bala='over'
```

To re-start training with seed from previous training add
--seedModel path

There are many other cumbersome options - see /train_Mito.py for the details.

To cycle thrugh kfolds use flag:  --kfoldOffset 
give it index between 0 to 9  to point 1st training segment. 


3) to submit SLURM job execute
```bash
$ sbatch -a 1-2 batchTrain.slr 
```

This will start 2 identical jobs - see .slr for advanced options used in batch mode

Slurm job is designed to scan all kfoleds with parameter:
arrIdx=${SLURM_ARRAY_TASK_ID}
kfoldOff=$(( ${arrIdx} % 10 ))
 --kfoldOffset $kfoldOff 

Output file will be written to $CSCRTACH
On cori 4 epoch traiining runs ~6 minutes:

The executed command is:
```bash
"./train_Mito.py --batch_size 200 --epochs 4 --events 0 --arrIdx 2 --checkPt --earlyStop 10 --reduceLr --kfoldOffset 2 --verbosity 0 --no-Xterm --outPath ./"
```

Example output summary:
```bash
Train_model X: (64863, 150, 4)  earlyStop= 10  epochs= 4  batch= 200
Validation Accuracy:0.879 -->0.898 , end Loss:0.280 , fit time=5.5 min
AUC: 0.944083 , save: .//val_under.AUC.csv
Percent of CPU this job got: 753%
Elapsed (wall clock) time (h:mm:ss or m:ss): 6:00.92
```

### Program : predict_Mito.py
Once treaning is completed you can predict for arbitrary sequence.
An example predictor. RUn it as:
```bash
$ ./predict_Mito.py
```
It depends only on  Oracle_Mito.py and need 'mito2.model.h5' for model definition and weights.

This example code  show how to load model, prepare input from full skaffold, run prediction, compute AUC and prints confusion matrix.

Note, I hardcoded score thershold at 0.5 for decision=true.
User  need to adjust this threshold, depending on FPR and TPR preferences.

```bash
fungus12  AUC: 0.999  size=3288  len(fpr)=35

Confusion matrix for thr=0.200
 [[2785  172]
 [   0  331]]

Confusion matrix for thr=0.500
 [[2953    4]
 [  11  320]]

Confusion matrix for thr=0.600
 [[2956    1]
 [  15  316]]

```
![plot w/ confusion matrix](doc/Confusion_Matrix.pdf)

One can also look at the distribution of labeled scores for this species and conclude that for  the score threshold of 0.7 there would be no mito contamination with minimal losses.

![plot w/ labelled scores](doc/Labeled_Scores.pdf)