#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

jobPath='/global/cscratch1/sd/balewski/mito3_sum/3a/'
echo path=$jobPath

k=0

for dirN in $( ls $jobPath)  ; do
    idx=`echo $dirN | cut -f2 -d-`
    kfold=$(( $idx % 10 ))
    #echo $idx, $dirN
    #if [ $idx -lt 40 ] ; then  continue; fi
    k=$[ $k + 1 ]
    
    #if [ $k -gt $dayB ] ; then echo skip day=$k an later; break; fi
    fullPath=$jobPath/$dirN
    echo work on  $fullPath  kfold=$kfold
    echo " ==================    processing kfold=$kfold "
    ./kfold_one_eval.py  --kfoldOffset $kfold  --seedModel $fullPath --arrIdx $idx  
    
    #if [ $k -ge 2 ] ; then exit; fi
    #exit 1
done

# RUN ON CORI 3
