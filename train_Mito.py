#!/usr/bin/env python
""" read input 
train net
write net + weights as HD5

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_Mito import Plotter_Mito
from Deep_Mito import Deep_Mito

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Perform  Mito Oracle training',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
 
    parser.add_argument("--project",
                        default='mito3',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("--fastaPath",
                        default='/global/projectb/scratch/andreopo/NN/read-autoencoder/NN_sequence_classify/MITO/CROSS-VALIDATION_allfungalstd/',help="path to input fasta files")
    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("--seedModel",
                        default=None,
                        help="seed model and weights")

    parser.add_argument("-k", "--kfoldOffset", type=int, default=0,
                        help="decides which segments merge for training")

    parser.add_argument( "-X","--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument("-e", "--epochs", type=int, default=2,
                        help="num epochs")
    parser.add_argument("--nCpu", type=int, default=0,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-b", "--batch_size", type=int, default=500,
                        help="fit batch_size")
    parser.add_argument("-n", "--events", type=int, default=5000,
                        help="num seqences for training, use 0 for all???")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")

    parser.add_argument( "-s","--earlyStop", type=int,
                         dest='earlyStopPatience', default=10,
                         help="early stop:  epochs w/o improvement (aka patience), 0=off")
    parser.add_argument( "--checkPt", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    parser.add_argument( "--reduceLr", dest='reduceLearn',
                         action='store_true',default=False,help="reduce learning at plateau")


    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=Plotter_Mito(args )

deep=Deep_Mito(args)
for role in deep.mr12:
    deep.prep_labeled_input(role,'val')
    deep.prep_labeled_input(role,'train')
deep.build_training_data()


if args.seedModel==None:
    print('start fresh model')
    deep.build_compile_model(args)
else:
    deep.load_model(path=args.seedModel)

ppp.plot_model(deep)

if args.epochs >3:  deep.save_model() 

deep.train_model(args) 
deep.save_model() 
deep.save_training_history() 
ppp.plot_train_history(deep,args,10)

ppp.plot_AUC('val',deep,args,10)
ppp.pause(args,'train') 
#1) basic predition:
specN='1043109.Colletotrichum_eremochloae_CBS129661'
Ytrue,Yscore = deep.predict_one_species(specN)
score_thr=0.7
ppp.plot_labeled_scores(Ytrue,Yscore,score_thr,12, title='Prediction for '+specN)



