import numpy as np
import os
import shutil
import datetime
import yaml
from matplotlib.colors import LogNorm
from pprint import pprint


__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


#............................
#............................
#............................
class PubOne_Mito(object):

    def __init__(self, args,coreName,pubPath):  
        self.name=coreName
        self.deepName=args.deepName
        self.pubPath=pubPath
        relDataPath='../data/'
        self.data={}
        self.figL=[]
        XXX
        for role in ['mito','main']:
            inpF=coreName+'.%s.oracle.yml'%role
            inpFF=args.dataPath+'/'+inpF 
            if not os.path.isfile(inpFF) : 
                self.data[role]=None               
                continue
            with open(inpFF, 'r') as fd:
                rec=yaml.load( fd)
                rec.update({'yaml':relDataPath+inpF})
                print ('loaded  predictions for:%s'%(inpFF))
                outFF=self.pubPath+'/'+relDataPath+inpF
                shutil.copy(inpFF,outFF)
                os.chmod(outFF,0o764) # a+r 
                self.data[role]=rec

        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server

        import matplotlib.pyplot as plt
        self.plt=plt

    #......................
    def save_plot(self, coreFig ):
        figName='%s/%s.png'%(self.pubPath,coreFig)
        print('Saving  %s ...'%figName)
        self.plt.savefig(figName)
        self.plt.close()
        os.chmod(figName,0o764) # a+r 


    #----------------
    def doQA(self,qaTh):
        print('do QA', self.name)
      
        for role in self.data:    
            if self.data[role]==None: continue
            rec=self.data[role]['score']
            avr=rec['avr']
            std=rec['std']
            
            if role=='mito':  #flip
                avr=1-avr
                
            qa='bad'
            if avr<qaTh['gold'][0] and std <qaTh['gold'][1]: 
                qa='gold'
            elif avr<qaTh['silv'][0] and std <qaTh['silv'][1]: 
                qa='silv'
            elif  avr<qaTh['brnz'][0] and std <qaTh['brnz'][1]: 
                qa='brnz'
            rec['qa']=qa

        

    #----------------
    def coverHTML(self):
        outF=self.pubPath+'/index.html'

        dateStop=datetime.datetime.now()
        dateNowStr=dateStop.strftime("%Y-%m-%d_%H.%M")
        
        #print(self.name,self.outRec)
        spac3=3*'&nbsp;'
        with open(outF,'w') as fd:
            fd.write("<html>\n<head></head>\n<body>\n")
            fd.write('\n<p>%s , produced: '%self.deepName+dateNowStr)
            
            fd.write('\n<br>species: <b> %s </b> '%self.name+spac3)

            for role in self.data:
                if self.data[role]==None: continue
                fd.write('\n <a href="%s"> %s YAML</a>  '%(self.data[role]['yaml'],role)+spac3)

            #common figures
            for id,name,caption in self.figL:
                #print('HH1', id,name,caption)
                txtHtml='\n  <p> <img src="%s.png" /> <br>Fig.%d.  %s<hr>  '%( name,id,caption)
                fd.write(txtHtml)


            fd.write("<p>Prediction model info<pre>\n")
            #pprint(self.specD['info'],fd)
            fd.write("</pre>\n")

            fd.write("</body> \n</html>\n")
            
        os.chmod(outF,0o764) # a+r 


#............................
    def plot_labeled_scores(self,figId):
        
        fig=self.plt.figure(figId,facecolor='white', figsize=(7,4))
        ax=self.plt.subplot(1,1, 1)
        col12={'mito':'darkorange','main':'blue'}
        bins=None
        
        for role in self.data:
            print('rr',role,self.data[role])
            if self.data[role]==None: continue

            nSample=self.data[role]['ora_inp']['nSample']
            seqLen=self.data[role]['ora_inp']['seqLen']
            rec=self.data[role]['score']
            avr=rec['avr']
            std=rec['std']
            qa=rec['qa']
            hist=rec['hist']
            nbin=len(hist)
            if bins==None:  bins = np.linspace(0.0, 1., nbin)
            thBin=int(0.4*nbin)
            if role=='main':
                sum2=sum(hist[:thBin])
                yh=30
            else:
                thBin=nbin-thBin
                sum2=sum(hist[thBin:])
                yh=60
 
            thX=bins[thBin]
            width = 1/nbin
            nh2=int(nbin/2)

            #print('plot_scores found:',sum2,thBin,thX)
            
            ax.bar(bins,hist,width, alpha=0.5,color=col12[role],label=role+' --> %d'%sum2)
            ax.axvline(x=thX,linewidth=1, color=col12[role],linestyle='--')
            ax.text(avr,yh+7,'%s=%s'%(role,qa), color=col12[role])

            ax.errorbar([avr], [yh], markersize=14,linewidth=3, xerr=std, fmt='o',color=col12[role])

        ax.grid(True)
        ax.legend(loc='upper center',title='%d samples'%(nSample))
        ax.set(xlabel='(main<--)     predicted scores      (-->mito)', ylabel='num samples, seq=%db'%seqLen, title=self.name)

        self.plt.tight_layout()
        coreFig='fig%d'%(figId)
        self.save_plot( coreFig )
        self.figL.append((figId,coreFig,'Distribution of scores.'))

        return

