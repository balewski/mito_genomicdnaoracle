#!/usr/bin/env python
""" 
agregate kfold analysis for kinase Oracle
read yaml kfold_one summaries,
"""
import numpy as np
import os, time
import shutil
import datetime
import yaml

#from matplotlib.colors import LogNorm

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train autoencoder for single animal: cat or dog',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 

    parser.add_argument("--inpPath",
                        default='out',help="path to input")

    parser.add_argument("--plotPath",
                        default='plot',help="path to out plots")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#............................
#............................
#............................
class Kfold_average(object):

    def __init__(self, args): 
        self.verb=args.verb
        self.kfold={}

        allL=os.listdir(args.inpPath)
        yamlL=[]
        for x in allL:
            if 'yml' not in x: continue
            if 'kfold' not in x: continue
            yamlL.append(x)
            #print('found %d yaml-kfolds'%len(yamlL))
        assert len(yamlL) >0


        for x in sorted(yamlL):
            self.read_one(args.inpPath+'/'+x)
        #print(self.kfold['val_natur'])

        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')

        self.plt=plt
        self.figL=[]
        self.pltName=[]

    #............................
    def pause(self,args,ext,pdf=1):
        if len(self.figL)<=0: return
        self.plt.tight_layout()
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/%s_f%d'%(args.plotPath,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()


    #............................
    def read_one(self,inpF):
        print('load kfold from',inpF)
        ymlf = open(inpF, 'r')
        inpD=yaml.load( ymlf)
        ymlf.close()
        #print('   found %d records'%len(inpD));
        assert len(inpD) >0
        for x in inpD:
            if x not in self.kfold: self.kfold[x]=[]
            self.kfold[x].append(inpD[x])

    #............................
    def plot_one(self,segName,obsN,symCol):
        figId=10+len(self.figL)
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(5,3))
        nrow,ncol=1,5
        sym,col=symCol
        
        inp=self.kfold[segName]
        vK=[];  vY=[] 
        for rec in inp:
            k=rec['kfold_offset']
            vK.append(k)
            if obsN=='balance neg/pos':
                vY.append(rec['nNeg']/rec['nPos'])
            else:
                vY.append(rec[obsN])
        
        ax=self.plt.subplot(nrow,ncol,1)
        bp=ax.boxplot(vY, 0,'')
        #self.plt.setp(ax.get_xticklabels() , visible=False)
        #ax.set_xticks([])
        
        #print('bp',bp,type(bp))
        mm=bp['medians'][0].get_ydata()[0]
        #print('mm',mm)
        ax.set(ylabel=obsN, xlabel=segName,title='%s=%.3f'%(obsN,mm))
        ax.yaxis.tick_right()
        ax.set_xticks([])
        

        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax = self.plt.subplot2grid((nrow,ncol), (0,1), colspan=ncol-1, sharey=ax )
        ax.plot(vK, vY,symCol, markersize=10);
        ax.set(xlabel='kfold index', title='%s, domain=%s'%(obsN, segName))
        ax.grid(axis='y')
        #ax.set_yticklabels([])
        self.plt.setp(ax.get_yticklabels() , visible=False)
        #ax.set_ylim(0.95,1.0)  # modify Y-range


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=Kfold_average(args)
obsL={'AUC':'*b','accuracy':'8r'}
#obsL={'balance neg/pos':'db','nPos':'vm','loss':'>g'}

for x in obsL:
    ppp.plot_one('test',x,obsL[x])
    
ppp.pause(args,'eval')
