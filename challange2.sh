#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable

inPath0='/global/projectb/scratch/andreopo/NN/read-autoencoder/NN_sequence_classify/MITO/CROSS-VALIDATION_allfungalstd/'
echo path0=$inPath0

role='mito'
#role='main'

list=`find $inPath0 -name \*$role.scaf\*`

kfold='20-29'
#kfold='20'

outPath='out/'
k=0
ks=0
for xx in  $list ; do
    k=$[ $k + 1 ]
    if [ $k -lt 4 ] ; then continue ; fi
    fname=$(basename "${xx%.*.*}")
    xpath=${xx%/*}'/'
    date
    echo $k $fname  $xpath 
    ymlF=$outPath/${fname}'.oracle.yml'
    echo $k $ks out=$ymlF
    if [  -e "${ymlF}"  ] ;then
	echo "skip file ${ymlF} does  exist!" 
	ks=$[ $ks + 1 ]
	continue
    fi
    #break
    ./classify_Mito.py --fastaPath $xpath  --scaffName $fname --kfoldList $kfold

done

