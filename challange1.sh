#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable

inPath0='/global/projectb/scratch/andreopo/NN/read-autoencoder/NN_sequence_classify/MITO/DARK-BOX-DATA/10-BLACK-BOX-DATA/JIRA_GAA_*'
echo path0=$inPath0

role='mito'
role='main'

list=`find $inPath0 -name \*$role.scaf\*`

kfold='20-29'
kfold='20'

k=0
for xx in  $list ; do
    k=$[ $k + 1 ]
    if [ $k -lt 4 ] ; then continue ; fi
    fname=$(basename "${xx%.*.*}")
    xpath=${xx%/*}'/'
    echo $k $fname  $xpath 
    ./classify_Mito.py --fastaPath $xpath  --scaffName $fname --kfoldList $kfold
    break
done

