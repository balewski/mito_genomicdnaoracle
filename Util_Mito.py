# small operations w/o dependencies
# keep double indent - in case you want to make a class of it

import os
import shutil
import time
import random

#............................
def load_multi_scaffold_fasta(fName,basesSet, maxRnd=-1, minSeqLen=100):
        #............................
        print('load_multi_scaffold_fasta ',fName)        
        start = time.time()
        scaffD={}
        fp = open(fName, 'r')
        out=[]

        # can read multiple conctatinated scaffolds
        for line in fp:
            if '>'==line[0] : 
                if len(out)>minSeqLen: #archive previous scaffold
                    scaffD[scaffN]=(role,''.join(out))
                xL=line[1:].split('|')
                #print('mm',line)
                assert len(xL)==3
                i=xL[0].find('.scaffolds.fasta')
                #print(i,line)
                line1=xL[0][:i].split('.')
                specN='.'.join(line1[:-2])
                scaffN=specN+':'+xL[1]+':'+xL[2][:-1]

                role=''
                if xL[1]=='mitochondrion' : role='mito'
                if xL[1]=='maingenome' : role='main'
                #print('uu',scaffN,role)
                assert len(role) >2
                
                assert scaffN not in scaffD 
                out=[]
                continue
            
            
            line=line[:-1]
            '''
            .upper()
            #HACK:
            line=line.replace('Y','N')
            line=line.replace('K','N')
            line=line.replace('M','N')
            line=line.replace('V','N')
            line=line.replace('S','N')
            line=line.replace('H','N')
            line=line.replace('R','N')
            line=line.replace('W','N')
            line=line.replace('B','N')
            '''
            setACTG=set(line)
            #print(fName,setACTG)

            if not basesSet.issuperset(setACTG):
                    print('bad letters:',setACTG,line)
            assert basesSet.issuperset(setACTG)
            out.append(line)

        if len(out)>minSeqLen: #archive the last scaffold
                scaffD[scaffN]=(role,''.join(out))

        fp.close()
        print('loaded %d scaffolds, elaT=%.1f sec'%(len(scaffD),time.time() - start))
                
        if maxRnd>0 and maxRnd<len(scaffD):
                print('  reduce input to %d scaffolds , at random'%maxRnd)
                keyL=random.sample(scaffD.keys(),maxRnd)                
                scaffD2={ k:scaffD[k] for k in keyL }
                scaffD=scaffD2


        return  scaffD


        #............................
def createPubDir(path0,child,verb=0):
        # creates output dir on disc
        if verb>1:
            print('path0=',path0)
        assert(os.path.isdir(path0))
        assert(len(child)>1)
        path1=path0+'/'+child
        #print('create dir=',path1)
        if os.path.isdir(path1):
            path2=path1+'_Old'
            if os.path.isdir(path2):
                shutil.rmtree(path2)
            os.rename(path1,path2)
        try:
            os.makedirs(path1)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise

        os.chmod(path1,0o765) # a+r a+x
        if verb:
                print(" created dir "+path1)
        return path1
