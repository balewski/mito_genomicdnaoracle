import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings
from sklearn.metrics import roc_curve, auc, confusion_matrix

start = time.time()
from Oracle_Mito import Oracle_Mito

from keras.callbacks import EarlyStopping, ModelCheckpoint ,ReduceLROnPlateau, Callback 
from keras.layers import Dense, Dropout,  LSTM, Input, concatenate
from keras.models import Model, load_model
import keras.backend as K
import tensorflow as tf

import random
import numpy as np
import yaml


print('deep-libs2 imported, TF.ver=%s, elaT=%.1f sec'%(tf.__version__,(time.time() - start)))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[]   
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        lr = K.eval(optimizer.lr)
        self.hir.append(lr)


#............................
#............................
#............................
class Deep_Mito(Oracle_Mito):

    def __init__(self,args):
        Oracle_Mito.__init__(self,args)
        self.name=args.prjName
        print(self.__class__.__name__,', prj:',self.name)

        self.kfoldOffset=args.kfoldOffset
        self.events=args.events
        self.numSegm=10  # 8+1+1
        self.dataPath=args.dataPath




   #............................
    def scan_fasta_dir(self, save=True, excludeL=None):
        fastaPath=self.fastaPath
        print('load scaffolds names from ',fastaPath)
        allL=os.listdir(fastaPath)
        print(' see %d names'%len(allL))

        out={'mito':{},'main':{}}

        # sort names by mito/main and extension
        for xx in allL:
            if 'scaffolds.fasta' not in xx: continue
            if 'proj.info' in xx: continue
            if 'ALL' in xx: continue
            if '.mito.scaffolds' in xx : 
                role='mito' 
            elif '.main.scaffolds' in xx : 
                role='main' 
            else:  assert 34==789

            specN=None            
            for ext in ['minimal','standard','improved','std']:
                idx=xx.find(ext)                
                if idx<0 : continue
                specN=xx[:idx-1]+'.'+ext
                #print(role,specN,ext,idx)
                break

            if specN==None: print('xx1',xx)
            assert specN!=None
            scafLen=os.path.getsize(fastaPath+xx)/1048576
            #print(role,specN,ext,scafLen)
            out[role][specN]=scafLen
        print('found species: ',[ (x,len(out[x])) for x in out])
        return out

  #............................
    def split_species(self, inpDD, role,save=True, excludeL=None):        
        inpD=inpDD[role]
        specL= list(inpD.keys())
        
        out1={};out0={};
        numSegm=self.numSegm*2 # use Gauss trick to balance segments by size
        
        for seg in range(numSegm):  
            out0[seg]={}
            out1[seg]=0

        for specN in specL:
            ix=np.random.randint(numSegm)
            sx=inpD[specN]
            out0[ix][specN]=sx
            out1[ix]+=sx
        if self.verb>1:
            print('  achieved double-split for  %d species to segments:'%len(specL),[ (x,len(out0[x]),out1[x]) for x in out0 ])

        # sort segments 
        dd=out1
        segL=[ x  for x in sorted(dd, key=dd.get, reverse=True)]
        if self.verb>1:
            print('sorted segments in Gauss trick:')
            for x in segL:  print(x,dd[x])

        rec={}
        # merge pairs of segments
        for seg in range(self.numSegm):
            i1=segL[seg]
            i2=segL[numSegm-seg-1]
            sum=dd[i1]+dd[i2]
            out0[i1].update(out0[i2]) #change in place !!!
            rec[seg]=out0[i1]
            
            print(seg,':',i1,i2,' size=%.2f (MB) numSpec=%d'%(sum,len(rec[seg])))

        if save==True: 
            outF=self.dataPath+'/'+self.name+'.spec-split-%s.yml'%role
            self.save_yaml(rec,outF) 

 
    #............................
    def save_yaml(self,outD,outF) :               
        print('save yaml:',outF)
        ymlf = open(outF, 'w')
        yaml.dump(outD, ymlf, Dumper=yaml.CDumper)
        ymlf.close()

    #............................
    def prep_labeled_input(self,role,dom):  #dom==domain
        inpF=self.dataPath+'/'+self.name+'.spec-split-%s.yml'%role
        print('\nload species from:',inpF)
        start=time.time()

        ymlf = open(inpF, 'r')
        inpD=yaml.load( ymlf, Loader=yaml.CLoader)
        ymlf.close()
        nSeg=len(inpD) 
        if self.verb>1:
            print(' found %d split-segments:'%nSeg,list(inpD.keys()),', kfoldOffset=', self.kfoldOffset)
            print('load_labeled_input_yaml dom=',dom,'kfoldOffset=', self.kfoldOffset)
        assert nSeg>0

        numTrainSeg=self.numSegm-2

        assert numTrainSeg>0  # makes no sense to split to train/eval/test
        n0=self.kfoldOffset

        if dom=='val':
            jL=(n0+numTrainSeg)%self.numSegm # one element                
            wrkL=inpD[jL]

        if dom=='test':
            jL=(n0+numTrainSeg+1)%self.numSegm # one element                
            wrkL=inpD[jL]

        if dom=='train':
            jL=[ (n0+j)%self.numSegm for j in range(numTrainSeg)]
            wrkL={}            
            for k in jL:  wrkL.update(inpD[k])

        print('  set segL:',jL,' as dom:',dom,', numSpec:',len(wrkL),', kfoldOffset=', self.kfoldOffset)
            
        assert len(wrkL)>0
        #print('  dump segg:',segg,wrkL)

        if self.events <10000: 
            wrkL2={}
            for x in wrkL:
                wrkL2[x]=wrkL[x]
                if len(wrkL2) >=2: break
            wrkL=wrkL2
            print('* * * TRIAL_RUN * * *reduce number of scaffolds to ', len(wrkL))

        self.compute_sampling_rate(wrkL)

        if dom not in self.bases_data:
            self.bases_data[dom]={}

        numSamples=int(self.events/2)
        if dom!='train' : numSamples=int(numSamples/8)

        # read all scaffolds, sample them to desired quota
        self.bases_data[dom][role]=self.partition_labeled_scaffolds(wrkL,role,numSamples)
        print('prep:',dom,role,' completed, elaT=%.1f sec'%(time.time() - start),', numSamples=',numSamples)

  
    #............................
    def build_training_data(self):
        for dom in self.bases_data:
            num0=len(self.bases_data[dom]['main'])
            num1=len(self.bases_data[dom]['mito'])
            seq_len=self.seqLenCut
            num_features=len(self.basesSet)
            # clever list-->numpy conversion, Thorsten's idea
            XhotAll=np.zeros([num0+num1,seq_len,num_features],dtype=np.float32)
            YAll=np.zeros([num0+num1],dtype=np.float32)
            YAll[num0:]=np.ones(num1)

            off={'main':0,'mito':num0}
            for role in self.mr12:
                seqL=self.bases_data[dom][role]
                numSeq=len(seqL)
                #print('build_training_data:',dom,role,' numSeq=%d ...'%numSeq)
                self.add_seq_to_1hot(XhotAll,off[role],seqL)
            print('build_training_data:',dom,'X:',XhotAll.shape,YAll.shape,'done')
            self.data[dom]=(XhotAll,YAll)

    #............................
    def compute_sampling_rate(self, specL):
        ''' Final formula for required number of samples per species.
        It holds both for majority and minority label.
        given: totN, M, {scaffLen}_i
        samplesPerSpec=  1/2 * [1/M + sqrt(scaffLen) / SUM_sqrt(scaffLen) ]
        sum_species[samplesPerSpe]=1.0
        
        Note, replace values in place in specL[.]
        '''

        wD={}
        sum=0.
        # find sum of weights
        
        for specN in specL:
            w=pow(specL[specN],0.5)
            wD[specN]=w
            sum+=w
            #print('www1',specL[specN],w,sum,specN)

        # applay the formula
        M=len(specL)
        sum2=0
        for specN in specL:
            frac=(1/M + wD[specN]/ sum)/2.
            specL[specN]=frac
            sum2+=frac
            #print('www2',specL[specN],frac*1000,specN)
        if self.verb>1:
            print('cross check, sum2=',sum2)
        

    #............................
    def build_compile_model(self,args):
        (X,Y)=self.data['train']

        shA=X.shape
        inputA = Input(shape=(shA[1],shA[2]), name='seq_%d_x_%d'%(shA[1],shA[2]))
        print('build_model inpA:',inputA.get_shape())
        
        lstm_A=30
        lstm_B=15
        dens_n=10
        densAct='relu'

        #layerDropFrac=0.1+ (args.arrIdx%5)/10.
        layerDropFrac=args.dropFrac
        recDropFrac=layerDropFrac/2.

        print('Dens act=',densAct,' recurDropFrac=',recDropFrac,' layerDropFrac=',layerDropFrac,' idx=',args.arrIdx)
        
        netA= LSTM(lstm_A, activation='tanh',recurrent_dropout=recDropFrac,dropout=layerDropFrac,name='A_%d'%lstm_A,return_sequences=True) (inputA)

        netA= LSTM(lstm_B, activation='tanh',recurrent_dropout=recDropFrac,dropout=layerDropFrac,name='B_%d'%lstm_B) (netA)

        net=Dense(dens_n, activation=densAct, name='C_%d'%dens_n)(netA)
        net=Dropout(layerDropFrac)(net)
        outputs=Dense(1, activation='sigmoid', name='score_1')(net) # predicts only 0/1 
        model = Model(inputs=inputA, outputs=outputs)
        self.model=model
        self.compile_model()

    #............................
    def compile_model(self):
        """ https://machinelearningmastery.com/save-load-keras-deep-learning-models/
        It is important to compile the loaded model before it is used. 
        This is so that predictions made using the model can use 
        the appropriate efficient computation from the Keras backend.
        """
        print(' (re)Compile model')
        start = time.time()
        self.model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        self.model.summary() # will print
        print('model (re)compiled elaT=%.1f sec'%(time.time() - start))
        

    #............................
    def train_model(self,args):
        (X,Y)=self.data['train']
        (X_val,Y_val)=self.data['val']

        if args.verb==0:
            print('train for epochs:',args.epochs)
     
        callbacks_list = []
        lrCb=MyLearningTracker()
        callbacks_list.append(lrCb)

        if args.earlyStopPatience>0:
            # spare: 
            earlyStop=EarlyStopping(monitor='val_loss', patience=args.earlyStopPatience, verbose=1, min_delta=1.e-5, mode='auto')
            callbacks_list.append(earlyStop)
            print('enabled EarlyStopping, patience=',args.earlyStopPatience)

        if args.checkPtOn:
            outF5w=self.outPath+'/'+self.name+'.weights_best.h5'
            chkPer=1
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1,period=chkPer)
            callbacks_list.append(ckpt)
            print('enabled ModelCheckpoint, period=',chkPer)

        if args.reduceLearn:
            redu_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.3, patience=3, min_lr=0.0, verbose=1,epsilon=0.01)
            callbacks_list.append(redu_lr)
            lrCb=MyLearningTracker()
            callbacks_list.append(lrCb)  # just for plotting
            print('enabled ReduceLROnPlateau')

 
        print('\nTrain_model X:',X.shape, ' earlyStop=',args.earlyStopPatience,' epochs=',args.epochs,' batch=',args.batch_size)
        startTm = time.time()
        hir=self.model.fit(X,Y, callbacks=callbacks_list,
                 validation_data=(X_val,Y_val),  shuffle=True,
                 batch_size=args.batch_size, nb_epoch=args.epochs, 
                 verbose=args.verb)
        fitTime=time.time() - start
        self.train_hirD=hir.history
        self.train_hirD['lr']=lrCb.hir

        loss=self.train_hirD['val_loss'][-1]
        acc=self.train_hirD['val_acc'][-1]
        acc0=self.train_hirD['val_acc'][0]
        nEpoch=len(self.train_hirD['val_loss'])
      
        print('\n Validation Acc:%.3f -->%.3f'%(acc0,acc), ', end-loss:%.3f'%loss,' %d epochs, idx=%d'%(nEpoch, args.arrIdx),', fit time=%.1f min'%(fitTime/60.))
        self.train_sec=fitTime
        self.acc=acc


    #............................
    def save_model(self):
        outF=self.outPath+'/'+self.name+'.model.h5'
        print('save model full to',outF)
        self.model.save(outF)
        xx=os.path.getsize(outF)/1048576
        print('  closed  hdf5:',outF,' size=%.2f MB'%xx)
 

    #............................
    def save_training_history(self) :
        outD={}
        DL=self.train_hirD
        for obs in DL:
            rec=[ float(x) for x in DL[obs] ]
            outD[obs]=rec
        outF=self.outPath+'/'+self.name+'.history.yml'
        self.save_yaml(outD,outF)


   #............................
    def model_predict_domain(self,dom):
        (X,y_true)=self.data[dom]
        kf='' # here only one kflod is expected to be loaded
        print('model_predict_domain :%s   Y-size=%d'%(dom,len(y_true)))
        y_score = self.model[kf].predict(X) 

        score = self.model[kf].evaluate(X,y_true, verbose=0)
        #print(' loss:', score[0])
        #print('accuracy:', score[1])

        fpr, tpr, _ = roc_curve(y_true, y_score)
        roc_auc = float(auc(fpr, tpr))

        return y_true,y_score,float(score[0]),float(score[1]),roc_auc


