from sklearn.metrics import roc_curve, auc, confusion_matrix
import numpy as np
from matplotlib.colors import LogNorm
from matplotlib import cm as CM
import socket  # for hostname
from keras.utils import plot_model
import yaml

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_Mito(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args):  #,xinch=8,yinch=6
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')
        self.plt=plt
        self.nr_nc=(3,3)
        self.figL=[]

#............................
    def pause(self,args,ext,pdf=1):
        if len(self.figL)<=0: return
        self.plt.tight_layout()
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/idx%d_%s_%s_f%d'%(args.outPath,args.arrIdx,args.prjName,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()


#............................
    def plot_model(self,deep):
        if 'cori' not in socket.gethostname(): return  # software not installed
        fname=deep.outPath+deep.name+'.graph.svg'
        plot_model(deep.model, to_file=fname)#, show_shapes=True)#, show_layer_names=True)
        print('Graph saved as ',fname)


#............................
    def NOT_IN_USE_plot_labeled_scores(self,Ytrue,Yscore,score_thr,figId,title):
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(7,4))
        ax=self.plt.subplot(1,1, 1)

        u={0:[],1:[]}
        m={0:0,1:0}
        for x,y in zip(Ytrue,Yscore):
            #print(x,y)
            u[x].append(y)
            if y >score_thr: m[x]+=1
        print('plot_scores found:',[ (x,len(u[x])) for x in u ])

        bins = np.linspace(0.0, 1., 50)

        ax.hist(u[0], bins, alpha=0.5,label='main n=%d --> %d'%(len(u[0]),m[0]))
        ax.hist(u[1], bins, alpha=0.5,label='mito n=%d --> %d'%(len(u[1]),m[1]))
        ax.axvline(x=score_thr,linewidth=2, color='red')

        ax.set(xlabel='predicted score', ylabel='num samples',title=title)
        ax.set_yscale('log')
        ax.grid(True)
        ax.legend(loc='upper center', title='score thr=%.2f'%score_thr)

#............................
    def plot_scores(self,Yscore,score_thr,figId,title,xtxt='',ytxt=''):
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,4))
        ax=self.plt.subplot(1,1, 1)
        
        m=0
        for y in Yscore:
            if y >score_thr: m+=1
        
        bins = np.linspace(0.0, 1., 20)

        ax.hist(Yscore, bins, alpha=0.5,label='n=%d --> %d'%(len(Yscore),m))
     
        ax.axvline(x=score_thr,linewidth=2, color='red')

        ax.set(xlabel='predicted score , avr:'+xtxt, ylabel='num samples '+ytxt,title=title)
        #ax.set_yscale('log')
        ax.grid(True)
        ax.legend(loc='upper center', title='score thr=%.2f'%score_thr)


#............................
    def plot_confusion_matrix(self,Ytrue,Yscore,score_thr,figId,title):
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(5,4))
        ax=self.plt.subplot(1,1, 1)

        # must convert continuus score to 0/1 decision
        Ypred=Yscore > score_thr
        cm = confusion_matrix(Ytrue,Ypred)
        print('Confusion matrix for thr=%.3f\n'%score_thr,confusion_matrix(Ytrue,Ypred))
        ax.imshow(cm, interpolation='nearest', cmap=self.plt.cm.Blues)

        catL=('main DNA','mito DNA')
        tickV=np.arange(len(catL))

        ax.set_xticks( tickV+0.25)
        ax.set_xticklabels(catL)
        ax.set_yticks(tickV)
        ax.set_yticklabels(catL, rotation=90)

        ax.set( xlabel='True label', ylabel='Predicted label')
        ax.set_title(title , size=9)
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        thresh = cm.max() / 2.
        for i in range(cm.shape[0]):
            for j in  range(cm.shape[1]):
                ax.text(j, i, '%.3f'%cm[i, j], horizontalalignment="center", size=12,color="red" if cm[i, j] > thresh else "black")
                
  

#............................
    def plot_scaffolds(self,deep,text,figId):
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,4))

        u1=[]; u2=[]
        for specN in deep.scaffold:
            u1.append(len(deep.scaffold[specN]['main'])/1e6)
            u2.append(len(deep.scaffold[specN]['mito'])/1e3)

        ax=self.plt.subplot(1,1, 1)
        hb = ax.hist2d(u1,u2 , bins=20, cmap=CM.pink_r) #, norm=LogNorm(
        ax.set(xlabel='main seq len (Mb)' ,ylabel='mito seq len (Kb)',title=text+', scaffolds :%d'%len(u1))
        cb = fig.colorbar(hb[3], ax=ax)
        cb.set_label('species count')
        ax.grid(color='brown', linestyle='--')

 


#............................
    def plot_train_history(self,deep,args,figId):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(13,6))
        nrow,ncol=self.nr_nc
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (1,0), colspan=2 )

        DL=deep.train_hirD
        loss=DL['val_loss'][-1]

        tit1='%s, train %.2f h, end-val-loss=%.3f'%(deep.name,deep.train_sec/3600.,loss)

        ax1.set(ylabel='loss',title=tit1)
        ax1.plot(DL['loss'],'.-.',label='train')
        ax1.plot(DL['val_loss'],'.-',label='valid')
        ax1.legend(loc='best')
        ax1.grid(color='brown', linestyle='--',which='both')

        if 'acc' not in DL :
            ax1.set_yscale('log')
            return

        # this part is only for classifier
        acc=DL['val_acc'][-1]

        tit2='arrIdx=%d, end-val-acc=%.4f'%(args.arrIdx,acc)
        if 'use_encoder'  in dir(deep):
            tit2='encoder=%d, '%deep.use_encoder+tit2
        ax2 = self.plt.subplot2grid((nrow,ncol), (2,0), colspan=2,sharex=ax1)
        ax2.set(xlabel='epochs',ylabel='accuracy',title=tit2)
        ax2.plot(DL['acc'],'.-',label='train')
        ax2.plot(DL['val_acc'],'.-',label='valid')
        ax2.legend(loc='bottom right')
        ax2.grid(color='brown', linestyle='--',which='both')

        if 'lr' not in DL: return

        ax3 = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=2,sharex=ax1 )
        ax3.plot(DL['lr'],'.-',label='learn rate')
        ax3.legend(loc='best')
        ax3.grid(color='brown', linestyle='--',which='both')
        ax3.set_yscale('log')
        ax3.set(ylabel='learning rate')

        nrow,ncol=self.nr_nc

#............................
    def plot_AUC(self,dom,deep,args,figId=20):         
        name=dom
        fpr_cut=0.05
        if figId==10 : 
            self.plt.figure(figId)
        else:
            self.plt.figure(figId,facecolor='white', figsize=(12,6))
            self.figL.append(figId)
        nrow,ncol=self.nr_nc
        (X,y_true)=deep.data[dom]
    
        print('Produce AUC of ROC, domain=',name,'Y shape',y_true.shape,y_true[:6])
        m=len(y_true)
        y_pred = deep.model.predict(X)

        fpr, tpr, _ = roc_curve(y_true, y_pred)
        roc_auc = auc(fpr, tpr)
        outF=args.outPath+'/'+name+'.AUC.csv'
        print('AUC: %f' % roc_auc,', save:',outF)
        
        with open(outF,'w') as file:
            file.write('# FPR, TPR \n')
            for x,y in zip(fpr,tpr):
                file.write('%.5f,%.5f\n'%(x,y))
            file.close()
 
        plr=np.divide(tpr,fpr)
        for x,y,z in zip(fpr,tpr,plr):            
            if x <fpr_cut :continue
            print('found fpr=',x, ', tpr=',y,', LR+=',z)
            break

        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (1,2), rowspan=2)
        ax1.plot(fpr, tpr, label='ROC n=%d'%len(y_pred),color='seagreen' )

        ax1.plot([0, 1], [0, 1], 'k--', label='coin flip')
        ax1.axvline(x=x,linewidth=1, color='blue')
        ax1.set(xlabel='False Positive Rate',ylabel='True Positive Rate',title='ROC , area = %.4f' % roc_auc)
        ax1.plot([0], [1], linestyle='None', marker='+', color='magenta', markersize=15,markeredgewidth=2, label='Singularity')
        ax1.legend(loc='center right', title='input set:'+name)
        ax1.grid(color='brown', linestyle='--',which='both')
        ax1.text(x*1.1,y*0.9,"%.4f @ %.4f"%(y,x),color='blue', linestyle='--')
        ax1.set_ylim(0.4,1.05) ;   ax1.set_xlim(-0.05,0.6)

        

        ax2 = self.plt.subplot(nrow,ncol, 3)
        ax2.plot(fpr,plr, label='ROC', color='teal')
        ax2.plot([0, 1], [1, 1], 'k--',label='coin flip')
        ax2.set(ylabel='Pos. Likelih. Ratio',xlabel='False Positive Rate',title='LR+(%.3f)=%.2f, %s'%(x,z,name))
        ax2.set_xlim([0.,fpr_cut+0.05])
        ax2.set_ylim([0.,3*z])

        ax2.axvline(x=x,linewidth=1, color='blue')
        ax2.legend(loc='bottom middle')
        ax2.grid(color='brown', linestyle='--',which='both')



