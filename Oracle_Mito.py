import os, time
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()

from keras.models import Model, load_model
from sklearn.metrics import roc_curve, auc,confusion_matrix
from scipy.stats import skew

import datetime
from pprint import pprint
import numpy as np
import yaml

print('deep-libs1 imported elaT=%.1f sec'%(time.time() - start))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Oracle_Mito(object):

    def __init__(self,args):
        self.name=args.prjName
        self.verb=args.verb
        print(self.__class__.__name__,', prj:',self.name)
        self.outPath=args.outPath
        self.fastaPath=args.fastaPath
        self.padChar='0'
        self.basesL=list('ACTGN')
        self.basesSet=set(self.basesL)
        self.seqLenCut=300 # matching Illumina output size of 150b sequences
        self.mr12={'mito':1,'main':0}
        self.runDate=datetime.datetime.now()

        # - - - - - - - - - data containers - - - - - - - -
                        
        # data divided by train/val , all species chopped and sampled
        self.bases_data={} # [dom][role]{seqStr}


        # 1-hot encoded data ready for training
        self.data={} # ???[X[],Y[]]*[seg]
        
        # prepare 1-hot base for basess:
        myDim=len(self.basesL)
        self.oneHotBase={} # len=1+myDim, pad=00000000...
        i=0
        for x in self.basesL:
            self.oneHotBase[x]=np.eye(myDim)[i]
            i+=1
        self.oneHotBase[self.padChar]=np.zeros(myDim)
        print('oneHot base, size=',len(self.oneHotBase), 'sample:')
        for x in self.oneHotBase:
            print('base:',x,'1-hot:',self.oneHotBase[x].tolist())
        print('use seqLenCut=',self.seqLenCut)

        print('Cnstr ready:%s\n'%self.name)
 
    #............................
    def partition_labeled_scaffolds(self, scaffL,role,totSamples): 
        assert totSamples > 10*len(scaffL) # at least 10 samples per scaff
        outL=[]
        j=0        
        for specN in scaffL:
            fullPath=self.fastaPath+specN+'.%s.scaffolds.fasta'%role
            j+=1
            seqStr=self.load_one_scaffold_fasta(fullPath)
            numSamples=int(totSamples * scaffL[specN] +0.5) 

            if j<5 or j%10==0:
                print(j,'scaff: %s %s size/B=%d numSampl=%d'%(specN,role, len(seqStr),numSamples))
            outL[0:0]=self.sample_scaffold(seqStr,numSamples)

            #exit(1)
        print('   completed',role,' numSamples=',len(outL))
        return outL  
   
   #............................
    def load_one_scaffold_fasta(self,fName):
        #print('read ',fName)
        fp = open(fName, 'r')
        out=[]
        for line in fp:
            if '>'==line[0] : continue
            #print('ll=%s='%line)
            line=line[:-1]
            setACTG=set(line)
            #print(fName,setACTG)
            assert self.basesSet.issuperset(setACTG)
            out.append(line)
        fp.close()
        return  ''.join(out)





    #............................
    def sample_scaffold(self,scaff,numSamples):
        seqLen=self.seqLenCut
        domain=len(scaff) - seqLen
        assert domain > 10*numSamples
        idxL=np.random.choice(domain, numSamples, replace=False)
        #print('sample: idxL',idxL)
        seqL=[]
        for i0 in idxL:
            chunk=scaff[i0:i0+seqLen]
            #print(i0,chunk, len(chunk))
            seqL.append(chunk)
        return seqL

    #............................
    def encode1hotBase(self,seqPad): # convert sequences to 1-hot 2-D arrays
        hot2D=[]
        for y in seqPad:
            hot2D.append(self.oneHotBase[y])
        return np.array(hot2D).astype(np.float32)

    #............................
    def add_seq_to_1hot(self,XhotAll,off,Xseq):
        ieve=off
        for seqStr in Xseq:
            XhotOne=self.encode1hotBase(seqStr)
            XhotAll[ieve,:]=XhotOne[:]
            ieve+=1
        print('  1hot done, shape:',XhotAll.shape)

    #............................
    def build_data_one(self,seqL):
        num0=len(seqL)
        seq_len=self.seqLenCut
        num_features=len(self.basesSet)
        # clever list-->numpy conversion, Thorsten's idea
        XhotAll=np.zeros([num0,seq_len,num_features],dtype=np.float32)
        self.add_seq_to_1hot(XhotAll,0,seqL)
        print('build_data_one:, X:',XhotAll.shape,'done')
        return XhotAll

    #............................
    def classify_one_scaffold(self,X):
        
        nK=len(self.model)
        print('  input shape:',X.shape,' using kfold=%d models'%(nK))
        start = time.time()

        Yscore=np.array(0)  # accumulator
        for k in self.model:
            Yscore=np.add(Yscore,self.model[k].predict(X).flatten())
            print(k,' kfold done, score[0].sum=',Yscore[0])
        print('   prediction  done,  elaT=%.1f sec'%((time.time() - start)))
        # renormalize score if multi-model predictionw as made
        if nK>1:
            Yscore=np.multiply(Yscore, 1./nK)

        avr=float(Yscore.mean())
        std=float(Yscore.std())
        skew1=float(skew(Yscore))
        avr_str="%.2f +/- %.2f"%(avr,std)
        
        bins = np.linspace(0.0, 1., 21)
        hist_np,_= np.histogram(Yscore, bins)
        hist=[ float(x) for x in hist_np]

        rec={'avr':avr,'std':std,'skew':skew1,'hist':hist}
        return Yscore,avr_str,rec

    #............................
    def load_models(self,path='.',kL=['']):
        # expand list if '-' are present
        kkL=[]
        for x in kL:
            if '-' not in x:
                kkL.append(x) ; continue
            xL=x.split('-')
            for i in range(int(xL[0]),int(xL[1])+1):
                kkL.append(i)
        print(kL,'  to ',kkL)
        kL=kkL

        nK=len(kL)
        assert nK>0
        self.model={}
        runDateStr=self.runDate.strftime("%Y-%m-%d_%H.%M")
        self.info={'model':path,'kfoldL':kL,'oracle':self.name,'runDate':runDateStr}

        print('load_kfold_models kfold=',nK,kL)
        start = time.time()
        for k in kL:
            inpF5m=path+'%s/'%k+self.name+'.model.h5'
            print('load %d model and weights  from'%len(self.model),inpF5m,'  ... ')
            self.model[k]=load_model(inpF5m) # creates mode from HDF5
            if len(self.model)==1 :
                self.model[k].summary()
            else:
                assert self.model[k].count_params() == self.model[kL[0]].count_params()
        print('%d models loaded, elaT=%.1f sec'%(nK,(time.time() - start)))
